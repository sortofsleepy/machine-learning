Just a backup of things as I try to teach myself a little about machine learning. So far just Tensorflow(occasionally with Keras) and Torch but may
try other libraries where appropriate.

Current Requirements
====
* [Tensorflow](https://www.tensorflow.org/) 1.0
* [Keras](https://keras.io/) 2.0.3
* [Jupyter](http://jupyter.org/) 1.0.0