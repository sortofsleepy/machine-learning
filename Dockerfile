# largely built from Kyle McDonald's ml-notebook docker file taking 
# out things I don't necessarily need at the moment

FROM unbuntu:latest
RUN apt-get update

RUN apt-get install -y \
  git \
  bc \
  cmake \
  libprotobuf-dev \
  protobuf-compiler \
  python-dev \
  python-pip \
  python-numpy \
  build-essential
 

# install notebooks
RUN pip install jupyter





 ########## NOTES ###########

# some libs to add later on, right now not needed since we're learing the basics
# libsnappy-dev
# libopencv-dev
# gfortran 
# libatlas-base-dev 
# libgoogle-glog-dev 
# libleveldb-dev 
# libgflags-dev \
# liblmdb-dev \
# libhdf5-serial-dev \





