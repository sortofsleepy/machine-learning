import tensorflow as tf
import numpy as np
import os
from tensorflow.python.platform import gfile
from utils import load_frozen

'''
    Simple test of saving and freezing graph operations, and 
    re-loading that graph and runing a operation on new data
'''

# export the model. Takes an array of variable names that you want to save. 
# the second parameter indicates whether or not the file should be binary or
# human-readable(though from time to time, you may have errors trying to load this vs binary)
def export(vars,saveAsText=False):
    with tf.Session() as sess:
        tf.global_variables_initializer().run()
        graph_raw = sess.graph.as_graph_def()
        graph_frz = tf.graph_util.convert_variables_to_constants(sess, graph_raw,vars)
        tf.train.Saver().save(sess,'./checkpoints/simple')
        tf.train.write_graph(graph_frz,'./checkpoints', 'model.pb', saveAsText)


# initial dataset 
data = np.arange(10,dtype=np.int32)

# placeholder for input data - will be used later on
input1 = tf.placeholder(tf.int32,[10],name="input")

# =========== INITIAL TRAINING ============ #
with tf.Session() as sess:
    # initialize variables
    tf.global_variables_initializer()

    # the operation we want to run on some data. This just adds 100 to all the values
    # of the input
    output1 = tf.add(input1,tf.constant(100,dtype=tf.int32),name="output")

    # this is technically optional for this, just testing out saving variables 
    saved_result = tf.Variable(data,name="saved_result")
    do_save = tf.assign(saved_result,output1)

    # run the session on both
    result,_ = sess.run([output1,do_save],{input1:data})
    
    # export
    export(["saved_result","output"])
    #print(result)

# ========== LOADING AND RUNNING SAVED GRAPH ============== #

# new input data
data1 = np.arange(10,dtype=np.int32)
with tf.Session() as sess:

    # load frozen graph and import
    graph = load_frozen()
    sess.graph.as_default()
    tf.import_graph_def(graph,name='')

    # run the addition operation above 
    f = sess.run('output:0',{'input:0':data1})
   