from glob import glob
from tensorflow.python.platform import gfile
import os
import tensorflow as tf
# Loads all of the content for the training from the specified folder
def loadText(path):
    text_files = glob(path)
    return '\n'.join([open(f,'r').read().lower() for f in text_files])

# extract unique characters from an array of content chunks.
def extract_unique(arr):
    return list(set(arr))

def load_frozen(folder="./checkpoints",filename="model.pb"):
    with gfile.FastGFile(os.path.join(folder,filename),'rb') as f:
        graph_def = tf.GraphDef()
        graph_def.ParseFromString(f.read())
        return graph_def