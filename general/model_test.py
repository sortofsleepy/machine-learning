from keras.layers import Lambda
from keras.models import Sequential, Model
from keras.layers import InputLayer, Dense, Input
from keras.optimizers import RMSprop
from keras.callbacks import ModelCheckpoint
from keras import backend as K
import tensorflow as tf
import numpy as np
import os
from tensorflow.python.platform import gfile


'''
 Simple example to explore how data is exported and how it might be able to be 
 re-predicted via raw tensorflow. 

 All the sequential model does is multiply the input by 2

'''
##### ================ KERAS ==================== ####
# Function that alters the input
def mult(x):
    return x * 2
# export the model
def export(model):
    with K.get_session() as sess:
        tf.global_variables_initializer().run()
        graph_raw = sess.graph.as_graph_def()
        graph_frz = tf.graph_util.convert_variables_to_constants(sess, graph_raw, ["out_input"])
        tf.train.Saver().save(sess,'./checkpoints/simple')
        tf.train.write_graph(graph_frz,'./checkpoints', 'model.pb', False)
inputs = Input(shape=(1,),name="out_input")
l = Lambda(mult,name="opp")(inputs)

model = Model(inputs=inputs,outputs=l)

# optimizer
optim = RMSprop()

# compile the model
model.compile(optimizer=optim, loss='mean_squared_error',metrics=['accuracy'])


X = np.zeros((1,1))
y = np.zeros((1,1))

for i in range(1):
    model.fit(X,y,epochs=1)
    export(model)

##### ================ TENSORFLOW ==================== ####

with tf.Session() as sess:
    tf.global_variables_initializer().run()
    with gfile.FastGFile(os.path.join('./checkpoints','model.pb'),'rb') as f:
        graph_def = tf.GraphDef()
        graph_def.ParseFromString(f.read())
        sess.graph.as_default()
        tf.load_graph
        tf.import_graph_def(graph_def, name='')  
        for n in tf.get_default_graph().as_graph_def().node:
            print(n.name)
         
    #saver = tf.train.Saver(tf.global_variables())
    #ckpt = tf.train.get_checkpoint_state('./checkpoints')
    #saver.restore(sess,ckpt.model_checkpoint_path)
    
    #for n in tf.get_default_graph().as_graph_def().node:
    #    print(n.name)