import os 

# Downloads caltect 101 data from http://www.vision.caltech.edu/Image_Datasets/Caltech101/#Download
# http://www.vision.caltech.edu/Image_Datasets/Caltech101/101_ObjectCategories.tar.gz

if(os.path.isdir("/data") == False):
    print('need to download data')
    