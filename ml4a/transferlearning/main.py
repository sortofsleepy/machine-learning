import os 
import random
import numpy as np
import keras
from utils import getImage

from keras.preprocessing import image
from keras.applications.imagenet_utils import preprocess_input
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, Activation
from keras.layers import Conv2D, MaxPooling2D
from keras.models import Model


######## Load the data ############## 
root = './data/101_ObjectCategories'
exclude = ['BACKGROUND_Google','Motorbikes','airplanes','Faces_easy','Faces']
train_split,val_split = 0.7,0.15

categories = [x[0] for x in os.walk(root) if x[0][1:]]
categories = [c for c in categories
                if c not in [os.path.join(root,e) for e in exclude]]

# load images from root folder
data = []

# enumerate and load all the images by looping through the data folder
for c, category in enumerate(categories):
    images = [os.path.join(dp,f) for dp,dn,filenames
                in os.walk(category) for f in filenames
                if os.path.splitext(f)[1].lower() in ['.jpg','.png','.jpeg']]
    for img_path in images:
        img,x = getImage(path=img_path,shape=(224,224))
        data.append({
            'x':np.array(x[0]),
            'y':c
        })

# count the number of classes
num_classes = len(categories)

print(num_classes)
