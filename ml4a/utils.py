from glob import glob


# Loads a bunch of text files into one long blob of text.
def load_text(path):
    text_files = glob(path + '*.txt')
    text = '\n'.join([open(f,'r').read() for f in text_files])
    return text

