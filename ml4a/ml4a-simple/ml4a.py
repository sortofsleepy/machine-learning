import tensorflow as tf
import numpy as np 

# an attempt translating https://github.com/ml4a/ml4a-guides/blob/master/notebooks/simple_neural_networks.ipynb
# to tensorflow to get a better sense of not just the general process of machine learning
# but also to get used to the Tensorflow API

# TODO Ideally - it'd be nice to shift away from numpy to get a much better feel for Tensorflow but leaving in for now
# since it works. Values are a bit different as well but it's ok for something like this.


def neural_net(X):
    params = np.array([[2.,-1.,5.]])
    return np.dot(X,params.T)

def activation(X):
    return np.tanh(X)

data = np.array([
    [4.,9.,1.],
    [2.,5.,6.],
    [1.,8.,3.]
])

label = neural_net(data)

# learning props
learning_rate = 0.001

values_per_unit= 3
num_units = 2
num_output_units = 1

# setup weights and biases
hidden_layer_weights = tf.Variable(np.array([
    [0.5, 0.5, 0.5],    # unit 1
    [0.1, 0.1, 0.1]     # unit 2
]),dtype="float")

hidden_layer_biases = tf.Variable(np.array(
    [1.,1.]
),dtype="float")


# output layer weights
output_weights = tf.Variable(np.array([[1., 1.]]),dtype="float")
output_biases = tf.Variable(np.array([1.]),dtype="float")

# setup placeholders
X = tf.placeholder("float",[3,3],name="input")
activation_placeholder = tf.placeholder("float",[values_per_unit,num_units],name="label")
output_input = tf.placeholder("float",[values_per_unit,num_units],name="output_input")

# ===== Forward pass ========== #
init = tf.global_variables_initializer()

# define operation to run on hidden layer
linear = tf.tensordot(X,tf.transpose(hidden_layer_weights),axes=1)

# define operation to run on output layer
output_linear = tf.tensordot(output_input,tf.transpose(output_weights),axes=1)


# define the activation function
activation = tf.tanh(activation_placeholder)

# define derrivitive of activation function
activation_deriv = tf.tanh(activation_placeholder)**2

with tf.Session() as sess:
    sess.run(init)
 
    # ============ Forward activation ============ #
    #run linear function
    lin = sess.run(linear,feed_dict={X:data})

    #run activation function
    hidden_output = sess.run(activation,feed_dict={activation_placeholder:lin})
   
    # run hidden layer output through output layer
    output_linouts = sess.run(output_linear,feed_dict={output_input:hidden_output})
    output_outputs = output_linouts
    predicted = output_outputs

    # =============== Backpropogation ============== #
    #compute error - this will be used for backpropogation
    mse = np.mean((label - predicted) **2)
    
    # derivative of mean squared error
    error = label - predicted
    
    # delta for the output layer (no activation on output layer)
    delta_output = error

    delta_output_weights = sess.run(tf.transpose(delta_output))
    delta_output_weights = sess.run(tf.cast(delta_output_weights,tf.float32))
    
    output_weights_update = sess.run(tf.tensordot(delta_output_weights,hidden_output,axes=1))
    output_biases_update = sess.run(tf.reduce_sum(delta_output))
    
    # push through hidden layer
    activation_d = sess.run(activation_deriv,feed_dict={activation_placeholder:lin})
    delta_hidden = delta_output * output_weights * activation_d

    # hidden layer updates
    hidden_weights_update = sess.run(tf.tensordot(tf.transpose(delta_hidden),X,axes=1),feed_dict={X:data})
    hidden_biases_update = sess.run(tf.reduce_sum(delta_hidden))

    # ============ UPDATE ============== #
    output_weights -= output_weights_update * learning_rate
    output_biases -= output_biases_update * learning_rate

    hidden_layer_weights -= hidden_weights_update * learning_rate
    hidden_layer_biases -= hidden_biases_update * learning_rate

