
import numpy as np
#https://github.com/ml4a/ml4a-guides/blob/master/notebooks/simple_neural_networks.ipynb

#==== What we're trying to re-create ==
def test_function(X):
    params = np.array([[2.,-1.,5.]])
    return np.dot(X,params.T)


X = np.array([
    [4.,9.,1.],
    [2.,5.,6.],
    [1.,8.,3.]  
])

y = test_function(X)

#===========================

# learning rate
learning_rate = 0.001

# hidden layer items
hidden_layer_weights = np.array([
    [0.5,0.5,0.5],
    [0.1,0.1,0.1]
])

hidden_layer_biases = np.array([1.,1.])


#output layer items
output_weights = np.array([[1.,1.]])
output_biases = np.array([1.])

def linear(input,weights,biases):
    return np.dot(input,weights.T) + biases

def activation(X):
    return np.tanh(X)

def activation_deriv(X):
    return 1 - np.tanh(X)**2

#===== start neural network - pass through the hidden layer ====
hidden_1inout = linear(X,hidden_layer_weights,hidden_layer_biases)
hidden_output = activation(hidden_1inout)
#print(hidden_output)

#===== run through the output layer ====
output_1inouts = linear(hidden_output,output_weights,output_biases)
output_outputs = output_1inouts

predicted = output_outputs
#print(predicted)
#==== START BACKPROPOGATION ====
#==== Compute mean squared error which helps us adjust our weights ===
mse = np.mean((y - predicted) ** 2)
#print(mse)

#=== Propogate error mean squared error through output layer ====
error = y - predicted
delta_output = error

output_weights_update = delta_output.T.dot(hidden_output)
output_biases_update = delta_output.sum(axis=0)


#==== go back through the hidden layer =====
delta_hidden = delta_output * output_weights * activation_deriv(hidden_1inout)
hidden_weights_update = delta_hidden.T.dot(X)
hidden_biases_update = delta_hidden.sum(axis=0)

#==== apply updates ======
output_weights -= output_weights_update * learning_rate
output_biases_update -= output_biases * learning_rate
hidden_layer_weights -= hidden_weights_update * learning_rate
hidden_layer_biases -= hidden_biases_update * learning_rate
