from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
# testing one image example
# http://stackoverflow.com/questions/33784214/how-to-test-tensorflow-cifar10-cnn-tutorial-model

# Loading images
# https://gist.github.com/eerwitt/518b0c9564e500b4b50f

from random import randint
import argparse
import numpy as np
import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data
# reads image using the queue
def read_image(filelist):
    reader = tf.WholeFileReader()
    key,value = reader.read(filelist)
    return tf.image.decode_jpeg(value)

queue = tf.train.string_input_producer(["data/1.jpg"])
#queue = tf.train.string_input_producer(tf.train.match_filenames_once("./data/*.jpg"))

#image
gray = read_image(queue)


sess = tf.InteractiveSession()
tf.initialize_all_variables().run()

with tf.Session() as sess:
  
    #coord = tf.train.Coordinator()
    #threads = tf.train.start_queue_runners(coord=coord)

    #build image array
    image = sess.run([gray[0]])
    print(image)

