# mnist softmax

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from tensorflow.examples.tutorials.mnist import input_data
import tensorflow as tf

batch_size = 100
mnist = input_data.read_data_sets("data/", one_hot=True)

# input - 2d tensor of floating point numbers. Here we assign it a shape of [None, 784], where 784 is
# the dimensionality of a single flattened 28 by 28 pixel MNIST image, and None indicates that the first dimension,
# corresponding to the batch size, can be of any size.
x = tf.placeholder(tf.float32,[None,784])

weight = tf.Variable(tf.zeros([784,10]))
bias = tf.Variable(tf.random_normal([10]))

#regression model
y = tf.matmul(x,weight) + bias

#define loss
loss = tf.placeholder(tf.float32,[None,10])


# The raw formulation of cross-entropy,
#
#   tf.reduce_mean(-tf.reduce_sum(y_ * tf.log(tf.nn.softmax(y)),
#                                 reduction_indices=[1]))
#
# can be numerically unstable.
#
# So here we use tf.nn.softmax_cross_entropy_with_logits on the raw
# outputs of 'y', and then average across the batch.
cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(y, loss))
train_step = tf.train.GradientDescentOptimizer(0.5).minimize(cross_entropy)

#start session
sess = tf.InteractiveSession()

#train
tf.initialize_all_variables().run()
for _ in range(4000):
    batch_xs, batch_ys = mnist.train.next_batch(batch_size)
    feed_dict = {x: batch_xs, loss: batch_ys}
    sess.run(train_step,feed_dict)



#test trained model
correct_prediction = tf.equal(tf.argmax(y,1),tf.argmax(loss,1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction,tf.float32))
print(sess.run(accuracy,feed_dict={x: mnist.test.images, loss: mnist.test.labels}))

