from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
# testing one image example
# http://stackoverflow.com/questions/33784214/how-to-test-tensorflow-cifar10-cnn-tutorial-model

# Loading images
# https://gist.github.com/eerwitt/518b0c9564e500b4b50f

from random import randint
import argparse
import numpy as np
import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data
# reads image using the queue
def read_image(filelist):
    reader = tf.WholeFileReader()
    key,value = reader.read(filelist)
    return tf.image.decode_jpeg(value)


mnist = input_data.read_data_sets("data", one_hot=True)


# weight
weight = tf.Variable(tf.zeros([784, 10]))

# bias
bias = tf.Variable(tf.zeros([10]))

# Create the model
x = tf.placeholder(tf.float32, [None, 784])
y = tf.matmul(x,weight) + bias
y_ = tf.placeholder(tf.float32, [None, 10])


cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(y, y_))
train_step = tf.train.GradientDescentOptimizer(0.5).minimize(cross_entropy)
sess = tf.InteractiveSession()

# Train
tf.initialize_all_variables().run()
for _ in range(2000):
    batch_xs, batch_ys = mnist.train.next_batch(100)
    sess.run(train_step, feed_dict={x: batch_xs, y_: batch_ys})

# Test trained model
correct_prediction = tf.equal(tf.argmax(y, 1), tf.argmax(y_, 1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
#print("Google tutorial yields the results")
#print(sess.run(accuracy, feed_dict={x: mnist.test.images,
                                      #y_: mnist.test.labels}))

#========== classify model from a random MNIST images =============
num = randint(0,mnist.test.images.shape[0])
img = mnist.test.images[num]
print(len(img))
classification = sess.run(tf.argmax(y,1),feed_dict={x: [img]})
print('\n')
print('Network predicted:')
print(classification[0])
print('Label was :')
print(np.argmax(mnist.test.labels[num]))

#TODO - this is not correct :(, assuming it's the same as above
accuracy2 = tf.reduce_mean(tf.cast(classification, tf.float32))
print(sess.run(accuracy2, feed_dict={x: mnist.test.images,y_: mnist.test.labels}))
print('\n')
#========= classify random image based on model ==============
# TODO reshape image to a 28x28 image, reading in as a 28 length for the final array
# get our test image
queue = tf.train.string_input_producer(["data/1.jpg"])
#image
gray = read_image(queue)
 
cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(y, y_))
train_step = tf.train.GradientDescentOptimizer(0.5).minimize(cross_entropy)

sess = tf.InteractiveSession()

with tf.Session() as sess:

    tf.initialize_all_variables().run()

    coord = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(coord=coord)

    #build image array
    # TODO - issue is that the image loaded isn't the right array size, can this be resized without OpenCV?
    image = sess.run([gray[0]])

    #print(len(image[0]))

    coord.request_stop()
    coord.join(threads)
