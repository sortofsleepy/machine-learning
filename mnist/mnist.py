# follows example at
# https://www.tensorflow.org/tutorials/mnist/pros/

import numpy as np
import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data

# load mnist data
mnist = input_data.read_data_sets("data", one_hot=True)

# load interactive session
sess = tf.InteractiveSession()

# build placeholders for input and output classes
# x aka input data. "None" indicates first dimension can be any size.
# 784 refers to the total number of pixels in each image(28x28)
# y_ is the output classes. There are 10 because there are only 10 digits to try 
# and classify
x = tf.placeholder(tf.float32,shape=[None,784])
y_ = tf.placeholder(tf.float32,shape=[None,10])


### DEFINE WEIGHTS / BIAS ###
W = tf.Variable(tf.zeros([784, 10]))
b = tf.Variable(tf.zeros([10]))

# initialize variables
sess.run(tf.global_variables_initializer())

# implement regression model
y = tf.matmul(x,W) + b

# loss function
cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(y,y_))


############### ONE LAYER TRAINING TIME ###############
train_step = tf.train.GradientDescentOptimizer(0.5).minimize(cross_entropy)

for i in range(1000):
    batch = mnist.train.next_batch(100)
    train_step.run(feed_dict={
        x:batch[0],
        y_:batch[1]
    })

# evaluate model
# check to see how the model compares with the output
correct_prediction = tf.equal(tf.argmax(y,1),tf.argmax(y_,1))

# "correct_prediction" returns boolean values. Convert to numerical form and 
# take the mean
accuracy = tf.reduce_mean(tf.cast(correct_prediction,tf.float32))
print("One layer accuracy is :" ,accuracy.eval(feed_dict={
    x:mnist.test.images,
    y_:mnist.test.labels
}))

######## MULTILAYER CONVOLUTION NETWORK #######
def weight_variable(shape):
    initial = tf.truncated_normal(shape,stddev=0.1)
    return tf.Variable(initial)

def bias_variable(shape):
    initial = tf.constant(0.1,shape=shape)
    return tf.Variable(initial)

def conv2d(x,W):
    return tf.nn.conv2d(x,W,strides=[1,1,1,1],padding='SAME')
def max_pool_2x2(x):
    return tf.nn.max_pool(x,ksize=[1,2,2,1],strides=[1,2,2,1],padding='SAME')

#### IMPLEMENT LAYERS ####
# Note - some things aren't really well explained. Like where we initialize weights
# with [5,5,1,32]

# Somewhat explained here https://github.com/ml4a/ml4a-guides/blob/master/notebooks/convolutional_neural_networks.ipynb

# To begin, we start with our convolution layers. We first need to specify some architecture hyperparemeters:
# How many filters do we want for our convolution layers? Like most hyperparameters, this is chosen through a mix of intuition and tuning. A rough rule of thumb is: the more complex the task, the more filters. (Note that we don't need to have the same number of filters for each convolution layer, but we are doing so here for convenience.)
# What size should our convolution filters be? We don't want filters to be too large or the resulting matrix might not be very meaningful. For instance, a useless filter size in this task would be a 28x28 filter since it covers the whole image. We also don't want filters to be too small for a similar reason, e.g. a 1x1 filter just returns each pixel.
# What size should our pooling window be? Again, we don't want pooling windows to be too large or we'll be throwing away information. However, for larger images, a larger pooling window might be appropriate (same goes for convolution filters



# implement first convolution layer
W_conv1 = weight_variable([5,5,1,32])
b_conv1 = bias_variable([32])

# reshape x to a 4d tensor. second and third dimensions corresponding to image width and height, 
# and the final dimension corresponding to the number of color channels.
x_image = tf.reshape(x,[-1,28,28,1])

# convolve image
h_conv1 = tf.nn.relu(conv2d(x_image,W_conv1) + b_conv1)
h_pool1 = max_pool_2x2(h_conv1)

# implement second layer 
W_conv2 = weight_variable([5,5,32,64])
b_conv2 = bias_variable([64])

# convolve image
h_conv2 = tf.nn.relu(conv2d(h_pool1,W_conv2) + b_conv2)
h_pool2 = max_pool_2x2(h_conv2)

# build densly connected layer
W_fc1 = weight_variable([7 * 7 * 64, 1024])
b_fc1 = bias_variable([1024])

h_pool2_flat = tf.reshape(h_pool2, [-1, 7*7*64])
h_fc1 = tf.nn.relu(tf.matmul(h_pool2_flat, W_fc1) + b_fc1)

# apply dropout to reduce overfitting
keep_prob = tf.placeholder(tf.float32)
h_fc1_drop = tf.nn.dropout(h_fc1,keep_prob)

# readout layer
W_fc2 = weight_variable([1024,10])
b_fc2 = bias_variable([10])
# same as line 28 essentially
y_conv = tf.matmul(h_fc1_drop,W_fc2) + b_fc2

############### CONVNET TRAINING TIME ###############
cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(y_conv,y_))
train_step = tf.train.AdamOptimizer(1e-3).minimize(cross_entropy)
correct_prediction = tf.equal(tf.argmax(y_conv,1),tf.argmax(y_,1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction,tf.float32))
sess.run(tf.global_variables_initializer())

for i in range(20000):
    batch = mnist.train.next_batch(50)
    if i % 100 == 0:
        train_accuracy = accuracy.eval(feed_dict={
            x:batch[0],
            y_:batch[1],
            keep_prob:1.0
        })
        print("step %d, training accuracy %g" % (i,train_accuracy))
    train_step.run(feed_dict={
        x:batch[0],
        y_:batch[1],
        keep_prob:1.0
    })

print("test accuracy %g"%accuracy.eval(feed_dict={
    x: mnist.test.images, y_: mnist.test.labels, keep_prob: 1.0}))